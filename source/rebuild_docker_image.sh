#!/bin/bash
#
# rebuild the Docker image of hadoop
#

image=$1
usage="Usage: sudo ./rebuild_docker_image.sh <hadoop_master|hadoop_slave|hadoop_base>"

if [ $# = 0 ]
then
	echo $usage
	exit 1
fi

# function for delete images
function docker_rmi()
{
	echo -e "\n\nsudo docker rmi -f yaolunlun/$1"
	sudo docker rmi -f yaolunlun/$1
}

# function for build images
function docker_build()
{
	cd $1
	echo -e "\n\nsudo docker build -t yaolunlun/$1 ."
	/usr/bin/time -f "real  %e" sudo docker build -t yaolunlun/$1 ./
	cd ..
}

echo -e "\ndocker rm -f slave1 slave2 master"
sudo docker rm -f slave1 slave2 master
sudo docker images > images.old

if [ $image == "hadoop_base" ]
then
	docker_rmi hadoop_master
	docker_rmi hadoop_slave
	docker_rmi hadoop_base
	docker_build hadoop_base
	docker_build hadoop_master
	docker_build hadoop_slave
elif [ $image == "hadoop_master" ]
then
	docker_rmi hadoop_master
	docker_build hadoop_master
elif [ $image == "hadoop_slave" ];
then
	docker_rmi hadoop_slave
	docker_build hadoop_slave
else
	echo $usage
fi

echo -e "\n----------old images"
cat images.old; rm images.old

echo -e "\n----------new images"
sudo docker images

exit 0

