#!/bin/bash

# run num slave containers
num=$1

# the defaut node number is 3
if [ $# = 0 ]
then
	num=3
fi
	
# delete master containers
echo "stop begin..."
sudo docker rm -f master &> /dev/null

# delete slave containers
i=1
while [ $i -lt $num ]
do
	sudo docker rm -f slave$i &> /dev/null
	((i++))
done 

echo "stop done..."

exit 0

